// import mongoose
const mongoose = require('mongoose');
// import modal
const orderModel = require('../model/orderModel');
const userModel = require('../model/userModel');
const drinkModel = require('../model/drinkModel');
const voucherModel = require('../model/voucherModel');
// create new order
const createNewOrder = (request, response) => {
    //b1 thu thập dữ liệu
    let userId = request.params.userId;
    let drinkId = request.params.drinkId;
    let voucherId = request.params.voucherId;
    let body = request.body;
    //b2 validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is invalid"
        })
    }
    else if (!body.orderCode) {
        response.status(400).json({
            message: "orderCode is required!"
        })
    }
    else if (!body.pizzaSize) {
        response.status(400).json({
            message: "pizzaSize is required!"
        })
    }
    else if (!body.pizzaType) {
        response.status(400).json({
            message: "pizzaType is required!"
        })
    }
    else if (!body.status) {
        response.status(400).json({
            message: "status is required!"
        })
    } else {
        let orderCreate = {
            _id: mongoose.Types.ObjectId(),
            orderCode: body.orderCode,
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            drink: body.drink,
            voucher: body.voucher,
            status: body.status,
        }
        //b3 làm việc với cơ sở dữ liệu
        orderModel.create(orderCreate, (error, data) => {
            if (error) {
                return response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                userModel.findById(userId,
                    {
                        push: { orders: data._id }
                    },
                    (err, updatedOrder) => {
                        if (err) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        } else {
                            return response.status(201).json({
                                status: "Create orders Success",
                                data: data
                            })
                        }
                    }
                )
            }
        });
    }
}
// get all orders
const getAllOrder = (request, response) => {

    //b3: thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get all orders success",
                data: data
            })
        }
    })
}
// get order by id
const getOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "order ID is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get order by id success",
                data: data
            })
        }
    })
}
//update order theo id
const updateOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let body = request.body;
    //b2: thu thập dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let orderUpdate = {
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status,
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}
// xóa user dựa vào id
const deleteOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order Id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete order success"
            })
        }
    })
}
// export controller methods
module.exports = {
    createNewOrder: createNewOrder,
    getAllOrder: getAllOrder,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
}